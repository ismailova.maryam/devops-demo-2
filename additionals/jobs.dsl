pipelineJob('demo-gitlab-local-triggered'){
  definition{
    cpsScm{
      scm{
        git{
          branch('master')
          remote{
            credentials('petclinic-gitlab-local-deploy-token')
            url('http://gitlab.local:8900/root/az-devops1')
          }
          extensions {
            cleanBeforeCheckout()
            relativeTargetDirectory {
              relativeTargetDir ("az-devops1")
            }
          }
        }
      }
      scriptPath('./az-devops1/Jenkinsfile')
    }
  }
  properties{
    pipelineTriggers{
      triggers{
        gitlab{
        	triggerOnPush(true)
          	includeBranchesSpec('master')
        }
      }
    }
  }
}
pipelineJob('demo-build-mysql'){
  definition{
    cpsScm{
      scm{
        git{
          branch('master')
          remote{
            credentials('petclinic-gitlab-local-deploy-token')
            url('http://gitlab.local:8900/root/az-devops1')
          }
          extensions {
            cleanBeforeCheckout()
            relativeTargetDirectory {
              relativeTargetDir ("az-devops1")
            }
          }
        }
      }
      scriptPath('./az-devops1/Jenkinsfile.mysql')
    }
  }
}