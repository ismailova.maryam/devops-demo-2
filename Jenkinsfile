pipeline {
   agent any
   environment{
    //    GITLAB
    // REGISTRYURL="https://registry.gitlab.com"
    // REGISTRY="registry.gitlab.com"
    // REGISTRYCREDS="petclinic-gitlab-remote-deploytoken"
    // REGISTRYUSER="/ismailova.maryam/az-devops1"
    // IMAGETAG="glreg"
    // GITLAB_CONNECTION="gitlab-ci-cd"
    // GITLAB_APITOKENNAME="petclinic-gitlab-remote-apitoken"
    // GITLAB_CHECKOUTCRED="petclinic-gitlab-remote-deploytoken"
    // GITLAB_URL="https://gitlab.com/ismailova.maryam/az-devops1"    
    //GENERAL
    DBIMAGENAME="petclinic-db"
    BUILDERNAME="maven:3-openjdk-11"
    PROJECTNAME="az-devops1"
    JAVAIMAGENAME="petclinic-app"
    // LOCAL
    REGISTRYURL="http://gitlab.local:5050"
    REGISTRY="gitlab.local:5050"
    REGISTRYCREDS="petclinic-gitlab-local-deploy-token"
    REGISTRYUSER="/root/az-devops1"
    IMAGETAG="lcreg"    
    GITLAB_CONNECTION="petclinic-gitlab-local"
    GITLAB_APITOKENNAME="petclinic-gitlab-local-apitoken"
    GITLAB_CHECKOUTCRED="petclinic-gitlab-local-deploy-token"
    GITLAB_URL="http://gitlab.local:8900/root/az-devops1"
    PETLCINICROOTPASSWORD=credentials('petclinic-root-password')
    PETCLINICUSRPASSWORD=credentials('petclinic-usr-password')
   }
   stages {
        stage('Checkout'){
            steps{
                copyArtifacts optional: true, projectName: "$JOB_NAME", selector: lastSuccessful(), target: '.'
                sh '! test -d ./.m2 && cp -r ~/.m2 ${WORKSPACE}/.m2 || exit 0'
            }   
        }
        stage('Build'){
            failFast true
            parallel{
                stage('Build Java') {
                    steps {
                        gitlabCommitStatus(connection: gitLabConnection(gitLabConnection: "$GITLAB_CONNECTION", jobCredentialId: "$GITLAB_APITOKENNAME"), name: 'build java') {
                            // Running maven package to build jar
                            sh 'docker container run -w /usr/src/maven  -v  ${WORKSPACE}/.m2:/root/.m2 -v ${WORKSPACE}/${PROJECTNAME}:/usr/src/maven maven:3-openjdk-11 mvn clean package'
                            sh 'docker image build -t $JAVAIMAGENAME:$IMAGETAG -f ${WORKSPACE}/${PROJECTNAME}/Dockerfile.java ${WORKSPACE}/${PROJECTNAME}/'
                            sh 'docker image tag $JAVAIMAGENAME:$IMAGETAG $REGISTRY$REGISTRYUSER/$JAVAIMAGENAME:$IMAGETAG'
                        }
                    }
                }
                stage('Build Mysql'){
                    // when database dockerfile is changed, rebuild the image
                    when{
                        anyOf{
                            changeset "Dockerfile.db";
                            changeset "setuser.sh"   
                        }
                    }
                    steps{
                        gitlabCommitStatus(connection: gitLabConnection(gitLabConnection: "$GITLAB_CONNECTION", jobCredentialId: "$GITLAB_APITOKENNAME"), name: 'build db') {
                            sh 'docker image build -t $DBIMAGENAME:$IMAGETAG -f ${WORKSPACE}/${PROJECTNAME}/Dockerfile.db ${WORKSPACE}/${PROJECTNAME}/'
                            sh 'docker image tag $DBIMAGENAME:$IMAGETAG $REGISTRY$REGISTRYUSER/$DBIMAGENAME:$IMAGETAG'
                        }
                    }
                }
            }
        }
        stage('Test'){
            // small change. Leave containers running 
            steps{
                gitlabCommitStatus(connection: gitLabConnection(gitLabConnection: "$GITLAB_CONNECTION", jobCredentialId: "$GITLAB_APITOKENNAME"), name: 'test') {
                    // stop process if running
                    sh 'docker-compose -f ${WORKSPACE}/${PROJECTNAME}/docker-compose.yml  down | exit 0'
                    sh 'docker-compose -f ${WORKSPACE}/${PROJECTNAME}/docker-compose.yml  up -d'
                    sh 'op=1;s=0;while [ $s -lt 100 ]; do sleep 2;  curl -s  localhost:8088/actuator/health | grep -q "UP" && op=0 && echo "$s SUCCESS" && break; s=$((s+2));done; test $op -eq 0'
                }
            }
            post{
                failure{
                    sh 'docker-compose -f ${WORKSPACE}/${PROJECTNAME}/docker-compose.yml  down | exit 0'
                }
            }
            // post{
            //     always{
            //          sh 'docker-compose -f ${WORKSPACE}/${PROJECTNAME}/docker-compose.yml  down | exit 0'
            //     }
            // }
        }
        stage('Push'){
            parallel{
                stage('Push database'){
                    when{
                        anyOf{
                            changeset "Dockerfile.db";
                            changeset "setuser.sh"   
                        }
                    }
                    steps{
                        gitlabCommitStatus(connection: gitLabConnection(gitLabConnection: "$GITLAB_CONNECTION", jobCredentialId: "$GITLAB_APITOKENNAME"), name: 'push db image') {
                            withDockerRegistry(credentialsId: "$REGISTRYCREDS", url: "$REGISTRYURL") {
                                sh 'docker image push $REGISTRY$REGISTRYUSER/$DBIMAGENAME:$IMAGETAG'    
                            }
                        }
                    }
                }
                stage('Push App'){
                    steps{
                        gitlabCommitStatus(connection: gitLabConnection(gitLabConnection: "$GITLAB_CONNECTION", jobCredentialId: "$GITLAB_APITOKENNAME"), name: 'push java image') {
                            withDockerRegistry(credentialsId: "$REGISTRYCREDS", url: "$REGISTRYURL") {
                                sh 'docker image push $REGISTRY$REGISTRYUSER/$JAVAIMAGENAME:$IMAGETAG'    
                            }
                        }
                    }
                }
            }           
        }
    }
    post {
        always {
            sh 'sudo chown -R jenkins:jenkins ${WORKSPACE}/'
        }
        success {
            archiveArtifacts allowEmptyArchive: true, artifacts: '.m2/**', followSymlinks: false, onlyIfSuccessful: true
        }
    }
}  
