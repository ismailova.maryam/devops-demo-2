# Demo-2 for DevOps course
## Setting Continuous Integration for Spring Petclinic project using Gitlab and Jenkins

### Table of contents:
* [Introduction](#introduction)
* [Docker](#docker)
* [Jenkins](#jenkins)
* [Gitlab](#gitlab)


### Introduction
The goal of this demo is to provide Continuous Integration for a Spring Petclinic project, which consists of a Java Spring app and a mysql database.
The build pipeline is implemented using Jenkins server, which is triggerred automatically as soon as a commit is pushed to master branch.

Even if the contents are pushed to this repository, the pipeline and CI were built locally using gitlab in docker container.

### Docker

There are two Dockerfiles in the current repository. One for MySQL database, which adds additional layer, and another one wrapping the jar file.
#### Java application

This dockerfile is built on the base of openjdk:8-slim, which is a slim version of openjdk:8. 

I provide the default envrionment variables, which have to be set for the application to run and connect to the database. Those are:
- `MYSQL_USER` - the name of user in mysql database
- `MYSQL_PASS` - the password for MYSQL_USER
- `MYSQL_URL` - url to connect ot mysql database

For security reasons application is run under a non-root user. for this purpose a new user '`appuser`' is created.

To build this image locally you need to have `.target/` directory in the project direcotory with the application jar file. The jar file can be built using maven locally or with a maven docker image (this strategy is also used in Jenkins pipeline).

#### MySQL 

Not much changes are added to default MySQL image, since it provides all the basic and required functionality. However, for additional security, to restrict connection to database by subnet, a `MYSQL_SUBNET` environment variable is set. The subnet restriction is set using setuser.sh script. This script is copied to `/docker-entrypoint-initdb.d/`, where the scripts for the startup can be placed.

As part of MySQL image, in order to persist database data  you need to bind a volume to `/var/lib/mysql` directory of the container.

### Jenkins

Jenkins server is started locally inside a Virtual Machine (through vagrant).

I have specified several credentials inside the Jenkins server to store the secrets to connect to gitlab and etc:

- `petclinic-gitlab-local-apitoken` - An access token to gitlab repository, with the scope of API. This one is necessary to update the status of commits in gitlab. (gitlabCommitStatus)
- `petclinic-gitlab-local-deploy-token` - A deploy token to the repository (with username, password) with the scope of: read_repository, read_registry, write_registry. This is used to connect to the registry where image will be stored( in the context of this demo image registry is integrated with gitlab, using minio object storage). 

Also there is a gitlab connection configured, which is used to trigger Jenkins pipeline and clone repositories and Jenkinsfiles.

- `petclinic-gitlab-local` - gitlab connection, which uses `petclinic-gitlab-local-apitoken` to connect.

The jobs are created using Job DSL plugin. The script for job creation can be found in `./additionals/jobs.dsl` . 

The primary pipeline job is `demo-gitlab-local-triggered`. This pipeline is triggered as soon as a commit is pushed to master branch on  `http://gitlab.local:8900/root/az-devops1`.  When triggered it clones the repository to az-devops1 directory in its workspace, and uses `Jenkinsfile` for the job.
Additionally I have set it to clean the directory from files that are untracked (according to `.gitignore`).

There is additional pipelineJob `demo-build-mysql`, which is not triggered by anything, but can be used to build mysql database image manually. It also retrieves the Jenkinsfile.mysql from the repository. This job might be useful, since mysql image is barely changed, or if commit with changes to mysql files was not processed by Jenkins. 

Jenkinsfile uses declarative syntax to define the pipeline job (`./Jenkinsfile`).  Pipieline consists of three main parts:
- environment - where I have defined all the variables used inside the pipeline, among which there are some credentials.
- stages - part where all the stages and steps are defined, the actual CI process.
- post - additional steps to do when the pipeline is complete.

The pipeline consists of 4 stages.
1. Checkout - the part where workspace directory is prepared, mainly local .m2 directory is created. (Beware, you need .m2 to be created on Jenkins machine's jenkin user. It can even be empty, but must exist).
    
    By default at this stage Jenkins will try to obtain an artifact of the latest successful build of a pipeline. The artifact here is the .m2 repository. 
    However for the first build, this artifact will not exist, and it will require to copy .m2 from jenkins user.

2. Build - the stage where Docker images are built. To do this faster images can be built in two parallel stages.
    
     MySQL image is only built if files related to mysql are changed in the commit, otherwise this step is skipped.

     Java application is built everytime. The build of jar file is done using maven docker container, where I mount my project source files. Maven container will built the jar and store it in target directory which is synced with bind volume. In order to reuse maven dependencies during the package step, I am mounting the .m2 directory of the current workspace.

    Once maven package has run successfuly and jar is ready, the docker image is built and tagged according to my registry settings. (`REGISTRYURL`, `REGISTRYUSER`, `IMAGETAG`).

3. Test - the stage where we test whether built images are working and interacting as expected.

    Testing is done using `docker-compose` file where everything is defined in services section. Once the services start and docker-compose up completes a query is sent to `localhost:8088` to test petclinic application. The query is sent during 100 seconds at most. If no successful response is received then images are not accepted and the pipeline is broken. 

    If the test is failed the post section will stop any started service.

4. Push - once all the stages are passed and successful we can push images to container registry in gitlab. The push is done parallelly as well.


Post section of the pipeline fixes permission issues of local .m2 directory, since the maven container changes the files to root user. 
After every successful pipeline .m2 repository is saved as an artifact.

### Gitlab

This section was required in order to do the integration of Jenkins with Gitlab. The setup is run as docker containers.

The docker-compose file for this setup can be found in `./additionals/docker-compose-minio.yml`.

 Minio object storage is integrated as container registry in gitlab. A minio client service is run to create a docker bucket in minio, which is then used by gitlab.