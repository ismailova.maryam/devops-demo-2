# escape=`
FROM openjdk:8-slim

ENV MYSQL_USER="petclinic" MYSQL_PASS="12345678" MYSQL_URL="jdbc:mysql://petclinic-db/petclinic" 

RUN useradd -m -s /bin/bash appuser && usermod -aG sudo appuser `
     && mkdir /home/appuser/jars && chown appuser:appuser /home/appuser/jars 

EXPOSE 8080

USER appuser

COPY --chown=appuser:appuser ./target/*.jar /home/appuser/jars

CMD java -jar -Dspring.profiles.active=mysql /home/appuser/jars/*.jar 



